#!/usr/bin/env bash

set -e

MASTER=`az repos ref list --project $AZURE_DEVOPS_PROJECT_NAME --repository $AZURE_DEVOPS_REPO_NAME --organization $AZURE_DEVOPS_ORG_SERVICE_URL --query "[?name=='refs/heads/master'].{objectId:objectId}" -o tsv`
echo "Master branch id : $MASTER"
# Create branch dev and staging. And set the dev to default branch.
az repos ref create --name "heads/dev" --project $AZURE_DEVOPS_PROJECT_NAME --repository $AZURE_DEVOPS_REPO_NAME --object-id $MASTER --organization $AZURE_DEVOPS_ORG_SERVICE_URL
az repos ref create --name "heads/staging" --project $AZURE_DEVOPS_PROJECT_NAME --repository $AZURE_DEVOPS_REPO_NAME --object-id $MASTER --organization $AZURE_DEVOPS_ORG_SERVICE_URL
az repos update --default-branch "dev" --project $AZURE_DEVOPS_PROJECT_NAME --repository $AZURE_DEVOPS_REPO_NAME --organization $AZURE_DEVOPS_ORG_SERVICE_URL

GROUP=`az devops security group list --organization $AZURE_DEVOPS_ORG_SERVICE_URL --project $AZURE_DEVOPS_PROJECT_NAME --query "graphGroups[?displayName=='Contributors'].{descriptor:descriptor}" -o tsv`

PROJECTID=`az devops project show --project $AZURE_DEVOPS_PROJECT_NAME --organization $AZURE_DEVOPS_ORG_SERVICE_URL --query 'id' -o tsv`
REPOSID=`az repos show --project $AZURE_DEVOPS_PROJECT_NAME --repository $AZURE_DEVOPS_REPO_NAME --organization $AZURE_DEVOPS_ORG_SERVICE_URL --query "id" -o tsv`
TOKEN="repoV2/$PROJECTID/$REPOSID"

# This string name master dev and staging in encoding (https://devblogs.microsoft.com/devops/git-repo-tokens-for-the-security-service/)
ENCODEDMASTER='6d0061007300740065007200'
ENCODEDDEV='640065007600'
ENCODEDSTATING='730074006100670069006e006700'

echo TOKEN : $TOKEN
echo GROUP : $GROUP

# Corresponding to the permission "Git Repositories"
PERMISSION_NAMESPACE=2e9eb7ed-3c0a-47d4-87c1-0ffdd275fd87

az devops security permission show --organization $AZURE_DEVOPS_ORG_SERVICE_URL --id $PERMISSION_NAMESPACE --subject $GROUP --token $TOKEN
az devops security permission update --organization $AZURE_DEVOPS_ORG_SERVICE_URL --allow-bit 8 --id $PERMISSION_NAMESPACE --subject $GROUP --token $TOKEN
az devops security permission update --organization $AZURE_DEVOPS_ORG_SERVICE_URL --deny-bit 8 --id $PERMISSION_NAMESPACE --subject $GROUP --token $TOKEN/refs/heads/$ENCODEDMASTER/
az devops security permission update --organization $AZURE_DEVOPS_ORG_SERVICE_URL --deny-bit 8 --id $PERMISSION_NAMESPACE --subject $GROUP --token $TOKEN/refs/heads/$ENCODEDDEV/
az devops security permission update --organization $AZURE_DEVOPS_ORG_SERVICE_URL --deny-bit 8 --id $PERMISSION_NAMESPACE --subject $GROUP --token $TOKEN/refs/heads/$ENCODEDSTATING/

